import React from 'react'
import { Workflow } from './Workflow'

import { render, cleanup, fireEvent, waitForElement, flushEffects } from 'react-testing-library'

const setUpTests = (props, fn = () => {}) => (
  <Workflow {...props}>
    {({ page, next, previous, to, first, last, flowState, isLastPage, update }) => (
      <div>
        <div data-testid="children">{page.content}</div>
        <div data-testid="state">{flowState}</div>
        <button onClick={to('page3')} data-testid="to" />
        <button onClick={update(fn('updated flowstate'))} data-testid="update" />
        <button onClick={to('pageDoesNotExist')} data-testid="pageDoesNotExist" />
        <button onClick={next} data-testid="next" />
        <button onClick={previous} data-testid="previous" />
        <button onClick={first} data-testid="first" />
        <button onClick={last} data-testid="last" />
        {isLastPage && <div data-testid="lastpage">Last Page Visible</div>}
      </div>
    )}
  </Workflow>
)
const flow = {
  page1: {
    content: 'Page 1 content',
  },
  page2: {
    content: 'Page 2 content',
  },
  page3: {
    content: 'Page 3 content',
  },
  page4: {
    content: 'Page 4 content',
  },
}

afterEach(cleanup)

it('Should pass through workflow and display first page', () => {
  const { queryByTestId } = render(setUpTests({ flow }))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
})

it('Should pass through workflow and display first page as per firstPage prop', () => {
  const firstPage = 1
  const { queryByTestId } = render(setUpTests({ flow, firstPage }))
  expect(queryByTestId('children').textContent).toBe('Page 2 content')
})

it('Should transition to next page when next button is pressed', () => {
  const { queryByTestId } = render(setUpTests({ flow }))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
  fireEvent.click(queryByTestId('next'))
  expect(queryByTestId('children').textContent).toBe('Page 2 content')
})

it('Should transition to previous page when previous button is pressed', () => {
  const firstPage = 1
  const { queryByTestId } = render(setUpTests({ flow, firstPage }))
  expect(queryByTestId('children').textContent).toBe('Page 2 content')
  fireEvent.click(queryByTestId('previous'))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
})

it('Should transition to page based on page name given', () => {
  const { queryByTestId } = render(setUpTests({ flow }))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
  fireEvent.click(queryByTestId('to')) // setUpTest calls page number
  expect(queryByTestId('children').textContent).toBe('Page 3 content')
})

test('Should not transtion "to" page when the given page does not exist', () => {
  const { queryByTestId } = render(setUpTests({ flow }))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
  fireEvent.click(queryByTestId('pageDoesNotExist'))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
})

it('Should not transition back if transitionBack flag set is set to false', () => {
  const transitionBack = false
  const firstPage = 1
  const { queryByTestId } = render(setUpTests({ flow, firstPage, transitionBack }))
  expect(queryByTestId('children').textContent).toBe('Page 2 content')
  fireEvent.click(queryByTestId('previous'))
  expect(queryByTestId('children').textContent).toBe('Page 2 content')
})

test('Should transition to last page', () => {
  const { queryByTestId } = render(setUpTests({ flow }))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
  fireEvent.click(queryByTestId('last'))
  expect(queryByTestId('children').textContent).toBe('Page 4 content')
})

test('Should transition to first page', () => {
  const firstPage = 2
  const { queryByTestId } = render(setUpTests({ flow, firstPage }))
  expect(queryByTestId('children').textContent).toBe('Page 3 content')
  fireEvent.click(queryByTestId('first'))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
})

test('Should not transition to to first page is transitionBack is set to false', () => {
  const transitionBack = false
  const firstPage = 2
  const { queryByTestId } = render(setUpTests({ flow, firstPage, transitionBack }))
  expect(queryByTestId('children').textContent).toBe('Page 3 content')
  fireEvent.click(queryByTestId('first'))
  expect(queryByTestId('children').textContent).toBe('Page 3 content')
})

test('Should not transition beyond last page when calling next', () => {
  const firstPage = 3
  const { queryByTestId } = render(setUpTests({ flow, firstPage }))
  expect(queryByTestId('children').textContent).toBe('Page 4 content')
  fireEvent.click(queryByTestId('next'))
  expect(queryByTestId('children').textContent).toBe('Page 4 content')
})

test('Should not transtion beyond first page when clicking previous page', () => {
  const { queryByTestId } = render(setUpTests({ flow }))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
  fireEvent.click(queryByTestId('previous'))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
})

test('Should pass lastPage props when last page in flow is render', () => {
  const firstPage = 1
  const { queryByTestId } = render(setUpTests({ flow, firstPage }))
  expect(queryByTestId('lastpage')).toBe(null)
  fireEvent.click(queryByTestId('last'))
  expect(queryByTestId('lastpage').textContent).toBe('Last Page Visible')
})

test('Should call beforeNextPage callback before rendering next page with current flowState', () => {
  const beforeNextPage = jest.fn()
  const initialState = [1, 2, 3]
  const { queryByTestId } = render(setUpTests({ flow, beforeNextPage, initialState }))
  fireEvent.click(queryByTestId('next'))
  expect(beforeNextPage).toBeCalledTimes(1)
  expect(beforeNextPage).toBeCalledWith(initialState)
})

test('Should not transition to next page if beforeNextPage return false', () => {
  const beforeNextPage = jest.fn(() => false)
  const { queryByTestId } = render(setUpTests({ flow, beforeNextPage }))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
  fireEvent.click(queryByTestId('next'))
  expect(beforeNextPage).toBeCalledTimes(1)
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
})

test('Should call transitionState as transition through pages and update flowState', () => {
  const transitionState = ({ flowState, active }) => [...flowState, `${active.content} - `]
  const initialState = ['initial state - ']
  const { queryByTestId } = render(setUpTests({ flow, transitionState, initialState }))
  fireEvent.click(queryByTestId('next'))
  fireEvent.click(queryByTestId('next'))
  expect(queryByTestId('state').textContent).toBe('initial state - Page 1 content - Page 2 content - ')
})

test('Should update flowState with value passed into update method', () => {
  const testUpdate = updateValue => ({ state }) => ({ state: [...state, updateValue] })
  const { queryByTestId } = render(setUpTests({ flow }, testUpdate))
  fireEvent.click(queryByTestId('update')) // update value in setUpTest = 'updated flowstate'
  expect(queryByTestId('state').textContent).toBe('updated flowstate')
})

test('Should update active page with value passed into udpate method', () => {
  const content = 'updated content'
  const testUpdate = _ => ({ page }) => ({ page: { ...page, content } })
  const { queryByTestId } = render(setUpTests({ flow }, testUpdate))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
  fireEvent.click(queryByTestId('update'))
  expect(queryByTestId('children').textContent).toBe(content)
})

test('Should transition to next page with ArrowRight key press', async () => {
  const { queryByTestId, container } = render(setUpTests({ flow }))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
  await waitForElement(() => fireEvent.mouseEnter(container.firstChild))
  flushEffects()
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowRight' }))
  expect(queryByTestId('children').textContent).toBe('Page 2 content')
})

test('Should transition to previous page with ArrowLeft key press', async () => {
  const { queryByTestId, container } = render(setUpTests({ flow }))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
  await waitForElement(() => fireEvent.mouseEnter(container.firstChild))
  flushEffects()
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowRight' }))
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowRight' }))
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowLeft' }))
  expect(queryByTestId('children').textContent).toBe('Page 2 content')
})

test('Should not transition to next page if preventKeyTransition is set to false', async () => {
  const preventKeyTransition = true
  const { queryByTestId, container } = render(setUpTests({ flow, preventKeyTransition }))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
  await waitForElement(() => fireEvent.mouseEnter(container.firstChild))
  flushEffects()
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowRight' }))
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowRight' }))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
})

test('Should not transition to next page if mouse has not entered flow', async () => {
  const { queryByTestId } = render(setUpTests({ flow }))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowRight' }))
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowRight' }))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
})

test('Should transition to next page if mouse has not entered flow but allowGlobalKeyTransition is set to true', async () => {
  const allowGlobalKeyTransition = true
  const { queryByTestId } = render(setUpTests({ flow, allowGlobalKeyTransition }))
  expect(queryByTestId('children').textContent).toBe('Page 1 content')
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowRight' }))
  await waitForElement(() => fireEvent.keyDown(document.body, { key: 'ArrowRight' }))
  expect(queryByTestId('children').textContent).toBe('Page 3 content')
})
