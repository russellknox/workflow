import React from 'react'
import { Workflow, makeFlow } from '../Workflow'
import { PageOne, PageTwo, PageThree } from './Pages'

import styled from 'styled-components'
const Flex = styled.div`
  display: flex;
  justify-content: space-around;
`
const Button = styled.div`
  display: flex;
  border-radius: 4px;
  justify-content: center;
  border: 2px solid black;
  min-width: 70px;
  padding: 10px;
  margin: 3px;
  cursor: pointer;
  &:hover {
    background: rgba(0, 0, 0, 0.2);
  }
`

const Flow = styled.div`
  width: 70vw;
  height: 60vh;
  margin: auto;
  border: 2px solid;
`
// take all pages and make them into a flow
// pass props into component
const flow = makeFlow(PageOne, PageTwo, PageThree)

const App = () => (
  <Workflow flow={flow}>
    {({ page: Page, next, previous, update, flowState }) => (
      <Flow>
        <div>State: {JSON.stringify(flowState, null, 2)}</div>
        <Page update={update} state={flowState} />
        <Flex>
          <Button onClick={previous}>Previous</Button>
          <Button onClick={next}>Next</Button>
        </Flex>
      </Flow>
    )}
  </Workflow>
)

export default App
