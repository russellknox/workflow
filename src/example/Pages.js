import React, { Fragment } from 'react'
import styled from 'styled-components'

const Section = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  width: 100%;
  height: 25%;
  background: steelblue;
  &:active {
    opacity: 0.6;
  }
`

const updateState = value => ({ state }) => ({
  state: {
    ...state,
    ...value,
  },
})

export const PageOne = ({ update }) => {
  return (
    <Fragment>
      <Section onClick={update(updateState({ pageOne: 'value one' }))}>Value One</Section>
      <Section onClick={update(updateState({ pageOne: 'value two' }))}>Value Two</Section>
      <Section onClick={update(updateState({ pageOne: 'value three' }))}>Value Three</Section>
    </Fragment>
  )
}
export const PageTwo = ({ update }) => {
  return (
    <Fragment>
      <Section onClick={update(updateState({ pageTwo: 'value three' }))}>Value three</Section>
      <Section onClick={update(updateState({ pageTwo: 'value four' }))}>Value four</Section>
      <Section onClick={update(updateState({ pageTwo: 'value five' }))}>Value five</Section>
    </Fragment>
  )
}
export const PageThree = ({ update }) => {
  return (
    <Fragment>
      <Section onClick={update(updateState({ pageThree: 'value six' }))}>Value six</Section>
      <Section onClick={update(updateState({ pageThree: 'value secven' }))}>Value seven</Section>
      <Section onClick={update(updateState({ pageThree: 'value eight' }))}>Value Eignt</Section>
    </Fragment>
  )
}
