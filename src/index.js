import React from 'react';
import ReactDOM from 'react-dom';
import Workflow from './example/Workflow';

ReactDOM.render(<Workflow />, document.getElementById('root'));