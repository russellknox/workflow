import React, { PureComponent } from 'react'
import propTypes from 'prop-types'

class Workflow extends PureComponent {
  static propTypes = {
    flow: propTypes.object.isRequired,
    firstPage: propTypes.number,
    transitionBack: propTypes.bool,
    beforeNextPage: propTypes.func,
    transitionState: propTypes.func,
    initialState: propTypes.any,
    preventKeyTransition: propTypes.bool,
    allowGlobalKeyTransition: propTypes.bool,
  }

  static defaultProps = {
    transitionBack: true,
    firstPage: 0,
    initialState: [],
    allowGlobalKeyTransition: false,
    preventKeyTransition: false,
  }

  state = {
    pages: this.props.flow,
    keys: Object.keys(this.props.flow),
    active: this.props.firstPage,
    flowState: this.props.initialState,
    mouseOverElement: false,
  }

  componentDidMount = () => {
    document.addEventListener('keydown', this.handleKeyDown)
  }

  componentWillUnmount = () => {
    document.removeEventListener('keydown', this.handleKeyDown)
  }

  handleMouseOverElement = () => {
    this.setState({ mouseOverElement: !this.state.mouseOverElement })
  }

  handleKeyDown = ev => {
    const { mouseOverElement } = this.state
    const { preventKeyTransition, allowGlobalKeyTransition } = this.props
    if (preventKeyTransition) return
    if (mouseOverElement || allowGlobalKeyTransition) {
      if (ev.key === 'ArrowRight') {
        this.next()
      }
      if (ev.key === 'ArrowLeft') {
        this.previous()
      }
    }
  }

  getActivePage = () => {
    const { keys, active } = this.state
    return keys[active]
  }

  getPagesLength = () => {
    return this.state.keys.length - 1
  }

  next = () => {
    const { active } = this.state
    const next = active + 1
    if (next > this.getPagesLength()) {
      this.setState({ active })
      return
    }
    this.beforeNextPage(next)
    this.updateState(next)
  }

  previous = () => {
    const { active } = this.state
    const previous = active - 1

    if (previous < 0 || !this.props.transitionBack) {
      this.setState({ active })
      return
    }
    this.updateState()
    this.setState({ active: previous })
  }

  to = page => () => {
    const { keys } = this.state
    const next = keys.indexOf(page)
    if (next < 0) return
    this.updateState()
    this.setState(() => ({ active: next }))
  }

  first = () => {
    if (!this.props.transitionBack) return
    this.setState(() => ({ active: 0 }))
  }

  last = () => {
    const { keys } = this.state
    this.setState(() => ({ active: keys.length - 1 }))
  }

  isLastPage = () => {
    return this.state.active === this.state.keys.length - 1
  }

  beforeNextPage = next => {
    const { flowState } = this.state
    const { beforeNextPage } = this.props

    if (beforeNextPage) {
      const transtion = beforeNextPage(flowState)
      if (transtion) {
        this.setState({ active: next })
        return
      }
      return
    }
    this.setState({ active: next })
  }

  updateState = next => {
    const { transitionState } = this.props
    if (!transitionState) return
    this.setState(({ flowState }) => ({
      flowState: transitionState({
        flowState,
        active: this.state.pages[this.getActivePage()],
      }),
      active: next,
    }))
  }

  update = value => _ => {
    const { flowState, pages } = this.state
    const active = this.getActivePage()
    const activePage = pages[active]

    const { page, state } = value({
      state: flowState,
      page: activePage,
    })

    const updatedPage = page ? page : activePage
    const updatedState = state ? state : flowState

    this.setState({
      flowState: updatedState,
      pages: {
        ...this.state.pages,
        [active]: updatedPage,
      },
    })
    return
  }

  setRenderProps = () => {
    const { pages, flowState } = this.state
    return {
      page: pages[this.getActivePage()],
      next: this.next,
      previous: this.previous,
      to: this.to,
      first: this.first,
      last: this.last,
      flowState,
      isLastPage: this.isLastPage(),
      update: this.update,
    }
  }

  render() {
    return (
      <div
        style={{ display: 'inline-flex' }}
        onMouseEnter={() => this.handleMouseOverElement()}
        onMouseLeave={() => this.handleMouseOverElement()}
      >
        {this.props.children(this.setRenderProps())}
      </div>
    )
  }
}

const makeFlow = (...components) =>
  components.reduce((state, Component) => {
    return {
      ...state,
      [Math.random() * 100]: props => <Component {...props} />,
    }
  }, {})

export { Workflow, makeFlow }
