### React Flow

Workflow lets you define a linear flow of components and/or data and provides the functionality to transition thorough the flow whilst having access to the content.

### Props
- `flow`: Array, is required. If you provide an array of components, there is a helper `makeFlow` function that takes and decorates them with the current flowState
- `transitionBack`: boolen. Defaults to false. Can restrict backwards transition
- `beforeNextPage`: async func that gets called before transition to next page happens
- `initialState`: defaults to array. You can define any initialState
- `transitionState`: Use to update state as you transtion through the flow. You get access to state and page content. i.e. use to document whether a page has been visited and what the page content was
- `preventKeyTransition`: default to false. Transtions through flow can be handled with arrow keys. Set to false to prevent this
- `allowGlobalKeyTransition`: default to false: Arrow keys will only work if mouse is over component. If you want to allow arrow keys without mouse being over component, set to true

### access to
- `page`: the current page of the flow. This will either contain data or a component. If you are using a component, use the makeFlow helper function as this will decorate the components with the transition state
- `next`: transition to next page
- `previous`: transition to previous page
- `to`: transition to given page. This can you page name (if known) or page number
- `first`: transition to start of flow
- `last`: transition to the end of flow
- `isLastPage`: boolan as to when you're on the last page
- `update` - allows you to updateState as you transition through the flow. This works similar to setState - you have access to the current state and you return the new desired state. This is usedful when wanting to record actions that happen whilst transtioning through the flow
- `flowState` - current state ofthe flow